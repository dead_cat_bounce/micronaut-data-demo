package com.te.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.te.model.User
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Produces
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.ArrayList
import javax.inject.Inject
import javax.sql.DataSource
import javax.transaction.Transactional

@Controller("/sql")
open class SqlController {
    @Inject
    lateinit var datasource: DataSource

    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    open fun getById(@PathVariable id: Int): String {
        val con: Connection = datasource.connection

        val sql: String = """
            |SELECT fname, lname, email
            |FROM user
            |WHERE id = ?
        """.trimMargin()
        val stmt: PreparedStatement = con.prepareStatement(sql)
        stmt.setInt(1,id)
        val rs: ResultSet = stmt.executeQuery()

        val users: ArrayList<User> = ArrayList()
        while (rs.next()) {
            val fname = rs.getString("fname")
            val lname = rs.getString("lname")
            val email = rs.getString("email")
            val usr = User(fname,lname,email)
            users.add(usr)
        }

        val json: String = when (users.size) {
            0 -> """{}"""
            1 -> {
                val mapper = ObjectMapper()
                mapper.writeValueAsString(users[0])
            }
            else -> """{"error":"Too many results found...hmmm...interesting"}"""
        }
        return json
    }
}