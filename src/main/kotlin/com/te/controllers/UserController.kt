package com.te.controllers

import javax.inject.Inject
import com.te.model.User
import com.te.repositories.UserRepository
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import java.util.*
import javax.sql.DataSource

@Controller("/user")
class UserController {
    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var datasource: DataSource

    @Get("/")
    @Produces(MediaType.TEXT_PLAIN)
    fun index(): String {

        if (::userRepository.isInitialized) {}
        val user: Optional<User> = userRepository.findById(1)
        if (user.isPresent) {
            val u2 = user.get()
            return "Found user nbr ${u2.id} who's name is ${u2.fname + " " + u2.lname}"
        } else {
            return "Hmmm....I got nuthin bud..."
        }
    }
}
