package com.te.controllers

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces

@Controller("ruok")
class RuokController {
    @Get("/")
    @Produces(MediaType.APPLICATION_JSON)
    fun index(): String {
        return """{"response":"imok"}"""
    }
}