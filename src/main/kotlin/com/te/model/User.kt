package com.te.model

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

@MappedEntity(value="user")
data class User(
    @field:Id @GeneratedValue
    val id: Int?,
    val fname: String,
    val lname: String,
    val email: String
) {
    constructor(fname: String, lname: String,email: String) : this(null, fname, lname, email)
}
