CREATE TABLE IF NOT EXISTS user (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fname VARCHAR(50),
    lname VARCHAR(50),
    email VARCHAR(256)
);
INSERT INTO user (fname, lname, email) VALUES ('Pete','Jones','pete.jones@example.com');
INSERT INTO user (fname, lname, email) VALUES ('Paula','Jones','notabdule@example.com.com');
INSERT INTO user (fname, lname, email) VALUES ('Efram','Jones','bigman@example.com.com');
INSERT INTO user (fname, lname, email) VALUES ('Indiana','Jones','fedora@example.com.com');
INSERT INTO user (fname, lname, email) VALUES ('Orlando','Jones','orlandj@example.com.com');
INSERT INTO user (fname, lname, email) VALUES ('Vegas','Jones','vjones@example.com.com');

