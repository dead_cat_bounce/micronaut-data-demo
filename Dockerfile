FROM ghcr.io/graalvm/graalvm-ce:java11-21.1.0 AS graalvm
RUN gu install native-image
COPY . /home/app
WORKDIR /home/app
RUN ./gradlew clean nativeImage
FROM frolvlad/alpine-glibc:alpine-3.12
RUN apk update && apk add libstdc++
COPY --from=graalvm /home/app/build/native-image/muhApp /app/muhApp
ENTRYPOINT ["/app/muhApp"]